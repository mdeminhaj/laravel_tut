@extends('layouts.admin')

@section('content')
<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Horizontal Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" name="packageFrm" id="packageFrm" action="" method="POST">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Email">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Price</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="price" name="price" placeholder="Password">
                     <input  type="hidden"  id="pid" name ="pid" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Description</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="description" name="description" placeholder="Email">
                  </div>
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>

          <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Responsive Hover Table</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th>Name</th>
                  <th>Price</th>
                  <th>Description</th>
                  <th>Action</th>
                  
                </tr>
                @foreach($p as $item)
                <tr>
               
                  <td>{{$item->name}}</td>
                  <td>{{$item->price}}</td>
                  <td>{{$item->description}}</td>
                  <td><button class="btn btn-warning edit" id="{{$item->id}}">Edit</button></td>
                </tr>
                @endforeach
                
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
@endsection
@section('page_scripts')
<script>
$('.edit').click(function () {
  var id=$(this).attr('id');
  var token = $('input[name="_token"]').val();
  var url="{{url('specificpackage')}}";
  $.ajax({
    url:url,
    method:"POST",
    data:{'id':id, '_token':token},
    success: function (result) {
      var obj=jQuery.parseJSON(result);
      $('#name').val(obj.name);
      $('#price').val(obj.price);
      $('#description').val(obj.description);
      $('#pid').val(obj.id);
      var newurl = "{{url('editpackage')}}";
      $('#packageFrm').attr('action',newurl);
    }
  });
});
</script>
@endsection