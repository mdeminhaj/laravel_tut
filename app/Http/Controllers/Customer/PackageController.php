<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Package;

class PackageController extends Controller
{
    public function show(){
        // $packages=App\Models\Package::all()
        $packages=Package::all();
        //dd($packages[0]->name);
        return view("customer.packages")->with('p', $packages);

    }

    public function save(Request $request){
        
        $package = new Package();
        $package->name=$request['name'];
        $package->price=$request['price'];
        $package->description=$request['description'];
        $package->save();

        return redirect('package');
        // dd($request);
        
    }

     public function getpackages(Request $request){
       //dd($request);
        $package = Package::find($request->id);


        return json_encode( $package);

    }
public function edit(Request $request){
        
        $package = Package::findOrFail($request->pid);
        $package->name=$request['name'];
        $package->price=$request['price'];
        $package->description=$request['description'];
        $package->save();

        return redirect('package');
        // dd($request);
        
    }
    
}
